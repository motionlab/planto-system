import redis 
import sys
import time
import datetime
import traceback
import random

r = redis.StrictRedis(host='localhost', port=6379, db=0)

while True:
  for sg in range(1,3):
    for fan in range(1,5):
      v = random.randint(20, 100)
      s = "/set/sg_{}/fan_{}/percentage".format(sg, fan)
      print(s)
      r.set(s, v)

  time.sleep(60 * 10) 
