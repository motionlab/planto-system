import datetime
import time
import os
import redis

from   influxdb_client                  import InfluxDBClient, Point, WritePrecision
from   influxdb_client.client.write_api import SYNCHRONOUS

influxdb_token  = os.getenv('INFLUXDB_TOKEN', 'AYm3QQwdNm4mbuYy7U2OJ7GJpbfDTiro2HtgyQfg9ZZ2RaO0-LJf1tms-2fd0F8BKb0lB6yhU_3XKCntWYiz5g==')
influxdb_org    = os.getenv('INFLUXDB_ORG',   'motionlab')
influxdb_bucket = os.getenv('INFLUXDB_BUCKET','planto')
influxdb_url    = os.getenv('INFLUXDB_URL',   'http://192.168.200.11:8086')
influxdb_client = InfluxDBClient(url=influxdb_url, token=influxdb_token, org=influxdb_org)
influxdb_write  = influxdb_client.write_api(write_options=SYNCHRONOUS)

r = redis.StrictRedis(host='localhost', port=6379, db=0)

while True:

  json_body = [
    {
        "measurement": "complete_system",
        "tags": {
            "host": "planto2",
            "region": "motionlab"
        },
        "time": datetime.datetime.utcnow(),
        "fields": {
        }
    }
       ]

  for key in r.scan_iter("*"):
    try:
      v = float(r.get(key).decode("UTF-8"))
      json_body[0]['fields'][key.decode("UTF-8")] = v 
    except:
        pass

  influxdb_write.write(influxdb_bucket, influxdb_org, json_body)
  r.setex("/system/influxer/active", datetime.timedelta(minutes=1), value=1)

  time.sleep(10)
