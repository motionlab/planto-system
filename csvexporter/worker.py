import os
import redis 
import sys
import csv
import time
import datetime
import traceback
import random

SYSTEM_KEYS  = ['/watercooling/watertemp', '/outside/temperature', '/inside/temperature']
SYSTEM_NAMES = ['timestamp', 'water_temp', 'outside_temp', 'inside_temp'] 
PLANTO_KEYS  = ["/lamp/active",
                "/lamp/temperature",
                "/fan_1/percentage",
                "/fan_2/percentage",
                "/fan_3/percentage",
                "/fan_4/percentage",
                "/lamp/current-out",
                "/lamp/voltage-out",
                "/lamp/power-out"
               ]
PLANTO_NAMES = ['timestamp', 'lamp_active', 'lamp_temp', 'fan_1', 'fan_2', 'fan_3', 'fan_4', 'lamp_current', 'lamp_voltage', 'lamp_power']

r        = redis.StrictRedis(host='localhost', port=6379, db=0)
now      = datetime.datetime.utcnow()
filebase = "{}/{}".format(sys.argv[1], datetime.datetime.utcnow().strftime('%Y-%m-%d'))


filename = '{}-system.csv'.format(filebase)

if not os.path.isfile(filename):
    with open(filename, 'w', newline='') as csvfile:
        system_writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        system_writer.writerow(SYSTEM_NAMES)

with open(filename, 'a', newline='') as csvfile:
    system_values = [now]

    for key in SYSTEM_KEYS:
        try:
            v = float(r.get(key).decode("UTF-8"))
            system_values.append(v)
        except:
            system_values.append("NaN")
    system_writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    system_writer.writerow(system_values)





for planto in range(1,3):
    filename = '{}-sg_{}.csv'.format(filebase, planto)

    if not os.path.isfile(filename):
        with open(filename, 'w', newline='') as csvfile:
            system_writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            system_writer.writerow(PLANTO_NAMES)

    with open(filename, 'a', newline='') as csvfile:
        planto_values = [now]

        for key in PLANTO_KEYS:
            try:
                k = "/sg_{}{}".format(planto, key)
                v = float(r.get(k).decode("UTF-8"))
                planto_values.append(v)
            except:
                system_values.append("NaN")
        system_writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        system_writer.writerow(planto_values)
