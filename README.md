# planto-system

## Message Universe

### System Status
- /system/danbus/active
- /system/sg_[1..2]/lamp/active
- /system/sg_[1..2]/fan/active
- /system/influxer/active
- /system/telegramer/active
- /system/orchestrater/active
- /system/co2/active

### Environment Sensoring
- /outside/temperature
- /inside/temperature
- /watercooling/pump-active
- /watercooling/radiator-active
- /watercooling/watertemp
- /door-open
- /voc
- /co2
- /humidity
- /pressure

### Subsystems
- /sg_[1..2]/lamp/model
- /sg_[1..2]/lamp/firmware
- /sg_[1..2]/lamp/active
- /sg_[1..2]/lamp/voltage-set
- /sg_[1..2]/lamp/current-set
- /sg_[1..2]/lamp/voltage-out
- /sg_[1..2]/lamp/current-out
- /sg_[1..2]/lamp/power-out
- /sg_[1..2]/lamp/temperature
- /sg_[1..2]/fan_[1..4]/percentage
- /sg_[1..2]/camera/light
- /co2/valve

### Sets
- /set/watercooling/pump
- /set/watercooling/radiator
- /set/sg_[1..2]/lamp/active
- /set/sg_[1..2]/lamp/voltage
- /set/sg_[1..2]/lamp/current
- /set/sg_[1..2]/fan_[1..4]/percentage
- /set/sg_[1..2]/camera/make-picture
- /set/sg_[1..2]/camera/light
- /set/telegram/send-message
- /set/co2/percentage

### States
- /day-cyclus

## URLS
- [DPS Python Gui](https://github.com/lambcutlet/DPS5005_pyGUI)
- [Lib to access DPS](https://github.com/Ultrawipf/pydps/blob/master/pydps.py)
