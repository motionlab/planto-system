import redis 
import sys
import time
import serial
import datetime
import traceback

if len(sys.argv) < 1:
    print("Error calling")
    sys.exit(1)

DEVICE_ID = sys.argv[1]
print(DEVICE_ID)


if int(DEVICE_ID) == 1:
    DEVICE_NAME = '/dev/planto-sg1'
elif int(DEVICE_ID) == 2:
    DEVICE_NAME = '/dev/planto-sg2'
else:
    print("Wrong device id")
    sys.exit(1)

r   = redis.StrictRedis(host='localhost', port=6379, db=0)

with serial.Serial(DEVICE_NAME, 115200, timeout=1) as ser:

    while True:
        line = ser.readline().decode("UTF-8")

        if len(line) < 5:
            continue

        elements = line.strip().split("=")
        print(elements)

        if len(elements) == 2:

          if elements[0] == "sg{}_temp".format(DEVICE_ID):
            r.setex("/sg_{}/lamp/temperature".format(DEVICE_ID),  datetime.timedelta(minutes=1), value=float(elements[1]))

        for i in range(1, 5):

            if r.exists("/set/sg_{}/fan_{}/percentage".format(DEVICE_ID, i)):
                raw = int(r.get("/set/sg_{}/fan_{}/percentage".format(DEVICE_ID, i)))
                r.delete("/set/sg_{}/fan_{}/percentage".format(DEVICE_ID, i))

                if raw < 0 or raw > 100:
                    continue

                v = int((255 * raw) / 100)
                s = "fan_{}={}\r\n".format(i, v)
                print(s)
                ser.write(s.encode())
                r.setex("/sg_{}/fan_{}/percentage".format(DEVICE_ID, i), datetime.timedelta(hours=1), raw)

        r.setex("/system/sg_{}/fan/active".format(DEVICE_ID), datetime.timedelta(minutes=1), value=1)

