# 28FFD04CA4164AB   ambient inside
# 28FFA731A4164A2   ambient outside
# 28FFB3A6A4165F8   Kuehlwasser Wassertank

import redis 
import sys
import time
import serial
import datetime

r = redis.StrictRedis(host='localhost', port=6379, db=0)

def getValues(ser):
  line = ser.readline().decode("UTF-8")

  if len(line) < 10:
    return

  elements = line.strip().split(',')

  if len(elements) == 2:

    if elements[0] == "28FFD04CA4164AB":
      r.setex("/inside/temperature",     datetime.timedelta(minutes=1), value=float(elements[1]))

    if elements[0] == "28FFA731A4164A2":
      r.setex("/outside/temperature",    datetime.timedelta(minutes=1), value=float(elements[1]))

    if elements[0] == "28FFB3A6A4165F8":
      r.setex("/watercooling/watertemp", datetime.timedelta(minutes=1), value=float(elements[1]))

    r.setex("/system/danbus/active", datetime.timedelta(minutes=1), value=1)

while True:

  try:
    with serial.Serial('/dev/planto-danbus', 115200, timeout=1) as ser:
      
      # clean up garbage
      for i in range(5):
        ser.readline()
      
      while True:
        getValues(ser)

  except KeyboardInterrupt:
    sys.exit(1)

  except:
    time.sleep(10)
