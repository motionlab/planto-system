import os
import sys
import datetime
from cv2 import *

if len(sys.argv) < 3:
    print("Error calling")
    sys.exit(1)

cam      = VideoCapture(int(sys.argv[1]))
s, img   = cam.read()
filebase = "{}/".format(sys.argv[2])

year   = datetime.datetime.utcnow().year
month  = datetime.datetime.utcnow().month
day    = datetime.datetime.utcnow().day
hour   = datetime.datetime.utcnow().hour
minute = datetime.datetime.utcnow().minute

if s:
    dir = "{}/{}".format(filebase, year)
    if not os.path.isdir(dir):
        os.mkdir(dir)

    dir = "{}/{}/{:02d}".format(filebase, year, month)
    if not os.path.isdir(dir):
        os.mkdir(dir)

    dir = "{}/{}/{:02d}/{:02d}".format(filebase, year, month, day)
    if not os.path.isdir(dir):
        os.mkdir(dir)

    filename = "{}/{}/{:02d}/{:02d}/{}-{:02d}-{:02d}-{:02d}-{:02d}.jpg".format(filebase, year, month, day, year, month, day, hour, minute)
    imwrite(filename, img) 

else:
    print("geht nicht")
