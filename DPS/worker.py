import redis 
import pydps
import sys
import os
import time
import datetime
import traceback

if len(sys.argv) < 1:
    print("Erro calling")
    sys.exit(1)

DEVICE_ID = sys.argv[1]

if int(DEVICE_ID) == 1:
    DEVICE_NAME = '/dev/planto-sg1-psu'
elif int(DEVICE_ID) == 2:
    DEVICE_NAME = '/dev/planto-sg2-psu'
else:
    print("Wrong device id")
    sys.exit(1)

r   = redis.StrictRedis(host='localhost', port=6379, db=0)
dps = pydps.dps_psu(DEVICE_NAME, 1)

def _init_connection():
    try:
        r.setex("/sg_{}/lamp/model".format(DEVICE_ID), datetime.timedelta(days=100), value=dps.getModel())
        r.setex("/sg_{}/lamp/firmware".format(DEVICE_ID), datetime.timedelta(days=100), value=dps.getFwVersion())
        return True
    except:
        return False

def _update_data():    
    try:
        if r.exists("/set/sg_{}/lamp/voltage".format(DEVICE_ID)):
            dps.setVoltage(float(r.get("/set/sg_{}/lamp/voltage".format(DEVICE_ID))))
            r.delete("/set/sg_{}/lamp/voltage".format(DEVICE_ID))

        if r.exists("/set/sg_{}/lamp/current".format(DEVICE_ID)):
            dps.setCurrent(float(r.get("/set/sg_{}/lamp/current".format(DEVICE_ID))))
            r.delete("/set/sg_{}/lamp/current".format(DEVICE_ID))

        if r.exists("/set/sg_{}/lamp/active".format(DEVICE_ID)):
            v = int(r.get("/set/sg_{}/lamp/active".format(DEVICE_ID)))
            if v > 0:
              dps.setOutput(True)
            else:
              dps.setOutput(False)
            r.delete("/set/sg_{}/lamp/active".format(DEVICE_ID))

        data = dps.getFullData()
        r.setex("/sg_{}/lamp/voltage-set".format(DEVICE_ID), datetime.timedelta(minutes=1), value=data['u-set'])
        r.setex("/sg_{}/lamp/current-set".format(DEVICE_ID), datetime.timedelta(minutes=1), value=data['i-set'])
        r.setex("/sg_{}/lamp/voltage-out".format(DEVICE_ID), datetime.timedelta(minutes=1), value=data['u-out'])
        r.setex("/sg_{}/lamp/current-out".format(DEVICE_ID), datetime.timedelta(minutes=1), value=data['i-out'])
        r.setex("/sg_{}/lamp/power-out".format(DEVICE_ID), datetime.timedelta(minutes=1), value=data['power'])
        
        if data['power'] > 0:
            r.setex("/sg_{}/lamp/active".format(DEVICE_ID), datetime.timedelta(minutes=1), value=1)
        else:
            r.setex("/sg_{}/lamp/active".format(DEVICE_ID), datetime.timedelta(minutes=1), value=0)
        r.setex("/system/sg_{}/lamp/active".format(DEVICE_ID), datetime.timedelta(minutes=1), value=1)
    except:
        traceback.print_exc()

if __name__ == "__main__":

    if not _init_connection():
        print ("Error connecting to DPS module")
        sys.exit(1)

    while True:
        _update_data()
        time.sleep(1)
