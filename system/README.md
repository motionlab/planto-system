# USB rulez
```bash
sudo cp 50-planto.rules /etc/udev/rules.d/
```

# Service
## Install
```
for i in danbus randomizer sg1 sg2 dps1 dps2
do
 sudo cp planto-${i}.service /lib/systemd/system/planto-${i}.service 
 sudo chmod 644              /lib/systemd/system/planto-${i}.service
 sudo systemctl daemon-reload
 sudo systemctl enable planto-${i}.service
 sudo systemctl start  planto-${i}.service
done
```

## Check
https://wiki.archlinux.org/title/systemd
```
sudo systemctl status hello.service
sudo systemctl start hello.service
sudo systemctl stop hello.service
sudo journalctl -f -u hello.service
```

